class Appointment {
  Appointment({
    required this.doctor,
    required this.patient,
    required this.dateTime,
    this.reason
  });

  final DateTime dateTime;
  final String doctor;
  final String patient;
  String? reason;

  factory Appointment.fromJson(Map<String, dynamic> json) {
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(
        json['dateTime'].seconds * 1000 + json['dateTime'].nanoseconds ~/ 1000000);

    return Appointment(
      patient: json['patient'],
      dateTime: dateTime,
      doctor: json['doctor'],
      reason:json['reason']
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'patient': patient,
      'doctor': doctor,
      'date': dateTime,
      'reason':reason
    };
  }
}
