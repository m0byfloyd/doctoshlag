import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../components/menu.dart';

class DoctorAppointmentChoice extends StatelessWidget{
  const DoctorAppointmentChoice({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const Menu(),
      appBar: AppBar(title: const Text('Choix de la vue calendrier')),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ElevatedButton(
              onPressed: () {
                context.push('/doctor-calendar/day');
              },
              child: Row(
                children: const [
                  Icon(Icons.date_range),
                  Text('Voir la vue jour')
                ],
              )
          ),
          ElevatedButton(
              onPressed: () {
                context.push('/doctor-calendar/week');
              },
              child: Row(
                children: const [
                  Icon(Icons.date_range),
                  Text('Voir la vue semaine')
                ],
              )
          ),
          ElevatedButton(
              onPressed: () {
                context.push('/doctor-calendar/month');
              },
              child: Row(
                children: const [
                  Icon(Icons.date_range),
                  Text('Voir la vue mois')
                ],
              )
          )
        ],
      ),
    );
  }

}