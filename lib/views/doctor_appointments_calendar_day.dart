import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:calendar_view/calendar_view.dart';

import '../app_state.dart';
import '../components/menu.dart';

class DoctorCalendarDay extends StatefulWidget {
  const DoctorCalendarDay({
    super.key,
  });

  @override
  State<DoctorCalendarDay> createState() => _DoctorCalendarDay();
}

class _DoctorCalendarDay extends State<DoctorCalendarDay> {
  @override
  Widget build(BuildContext buildContext) {
    return CalendarControllerProvider(
        controller: EventController(),
        child: Scaffold(
          appBar: AppBar(title: const Text('Calendrier')),
          drawer: const Menu(),
          body: Consumer<ApplicationState>(
            builder: (BuildContext context, appState, Widget? child) {
              if (!appState.loggedIn || appState.selectedDoctor == null) {
                buildContext.go('/');
              }

              List<CalendarEventData<Object>> events = [];

              if (appState.selectedDoctorAppointments.isNotEmpty) {
                for (final appointment in appState.selectedDoctorAppointments) {
                  events.add(CalendarEventData(
                      title: 'RDV avec le patient ${appointment.patient}',
                      date: appointment.dateTime,
                      startTime: appointment.dateTime,
                      description:
                          'Le patient ${appointment.patient} a rendez-vous avec le docteur ${appointment.doctor} pour ${appointment.reason}',
                      endTime:
                          appointment.dateTime.add(const Duration(hours: 50))));
                }
              }

              for (final event in events) {
                CalendarControllerProvider.of(context).controller.add(event);
              }

              return DayView(
                onEventTap: (event, date) => _dialogBuilder(context, event),
              );
            },
          ),
        ));
  }

  Future<void> _dialogBuilder(BuildContext context, event) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title:
              Text('Rendez-vous ${DateFormat.Hm().format(event[0].startTime)}'),
          content: Text(event[0].description),
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Text('Compris'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
