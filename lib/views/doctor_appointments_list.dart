import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../app_state.dart';
import '../components/menu.dart';

class DoctorAppointmentList extends StatefulWidget {
  const DoctorAppointmentList({
    super.key,
  });

  @override
  State<DoctorAppointmentList> createState() => _DoctorAppointmentList();
}

class _DoctorAppointmentList extends State<DoctorAppointmentList> {
  @override
  Widget build(BuildContext buildContext) {
    return Scaffold(
      drawer: const Menu(),
      appBar: AppBar(title: const Text('Liste des prochain rendez vous du docteur')),
      body: Column(
        children: [
          Consumer<ApplicationState>(builder: (context, appState, _) {
            if (!appState.loggedIn || appState.selectedDoctor == null) {
              buildContext.go('/');
            }
            return Column(children: [
              Text(
                  'Les rendez vous de ${appState.selectedDoctor?.name}'),
              for (var appointment in appState.selectedDoctorAppointments)
                Card(
                  child: Row(children: [
                    Row(
                      children: [
                        const Icon(Icons.access_time),
                        Text(
                            'Le ${DateFormat.EEEE().format(appointment.dateTime)} ${DateFormat.d().format(appointment.dateTime)} ${DateFormat.MMMM().format(appointment.dateTime)} ${DateFormat.M().format(appointment.dateTime)} à ${DateFormat.Hm().format(appointment.dateTime)}'),
                      ],
                    ),
                    Row(
                      children: [
                        const Icon(Icons.person),
                        Text(appointment.patient),
                      ],
                    )
                  ]),
                )
            ]);
          })
        ],
      ),
    );
  }
}
