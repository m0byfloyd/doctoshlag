import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../models/appointment.dart';

class AppointmentList extends StatefulWidget {
  const AppointmentList({
    super.key,
    required this.appointments,
  });

  final List<Appointment> appointments;

  @override
  State<AppointmentList> createState() => _AppointmentList();
}

class _AppointmentList extends State<AppointmentList> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        for (var appointment in widget.appointments)
          Text(
              textAlign: TextAlign.center,
              '${appointment.dateTime} : ${appointment.patient} - ${appointment.doctor} €'
          )
      ],
    );
  }
}
